from random import randint
from copy import deepcopy
from typing import List, Union
from time import sleep
import os


class GameOfLife:
    def __init__(self, tps: Union[int, float]=10, size: int=20):
        self.tps = tps
        self.size = size
        self.state = [[randint(0, 1) for _ in range(self.size)] for _ in range(self.size)]


    def count_alive_neighbors(self, row_idx: int, col_idx: int):
        c = 0
        # upX
        if row_idx > 0:
            # up
            c += self.state[row_idx - 1][col_idx]
            # up left
            if col_idx > 0:
                c += self.state[row_idx - 1][col_idx - 1]
            # up right
            if col_idx < self.size - 1:
                c += self.state[row_idx - 1][col_idx + 1]
        # downX
        if row_idx < self.size - 1:
            # down
            c += self.state[row_idx + 1][col_idx]
            # down left
            if col_idx > 0:
                c += self.state[row_idx + 1][col_idx - 1]
            # down right
            if col_idx < self.size - 1:
                c += self.state[row_idx + 1][col_idx + 1]
        # left
        if col_idx > 0:
            c += self.state[row_idx][col_idx - 1]
        if col_idx < self.size - 1:
            c += self.state[row_idx][col_idx + 1]
        return c

    def is_alive(self, row_idx: int, col_idx: int):
        """
        if alive and 2 or 3 living neighbors: alive
        if dead and 3 living neighbors: alive
        else: dead
        """
        alive = self.state[row_idx][col_idx]
        alive_neighbors = self.count_alive_neighbors(row_idx, col_idx)
        if alive_neighbors == 3:
            return 1
        elif alive and alive_neighbors == 2:
            return 1
        else:
            return 0

    def tick(self):
        new_state = [[0] * self.size for _ in range(self.size)]
        for row_idx in range(self.size):
            for col_idx in range(self.size):
                new_state[row_idx][col_idx] = self.is_alive(row_idx, col_idx)
        self.state = new_state

    def draw_board(self):
        os.system('clear')
        for row in self.state:
            print(''.join(['██' if cell else '  ' for cell in row]))

    def play(self):
        while True:
            self.draw_board()
            self.tick()
            sleep(1 / self.tps)

x = GameOfLife(60, 60)
x.play()
