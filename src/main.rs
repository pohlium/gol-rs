use rand::Rng;
use std::{thread, time};

struct GameOfLife {
    tps: u64,
    size: usize,
    state: Vec<Vec<bool>>,
}

fn create_random_state(size: usize) -> Vec<Vec<bool>> {
    let mut state = Vec::new();
    for _ in 0..size {
        let mut row = Vec::new();
        for _ in 0..size {
            let cell = rand::thread_rng().gen_bool(0.5);
            row.push(cell);
        }
        state.push(row);
    }
    state
}

impl GameOfLife {
    fn new(tps: u64, size: usize) -> Self {
        GameOfLife {
            tps,
            size,
            state: create_random_state(size),
        }
    }

    fn draw_board(&self) {
        print!("\x1B[2J\x1B[1;1H"); // clear screen
        for row in self.state.iter() {
            for cell in row.iter() {
                let output = match cell {
                    false => "  ",
                    true => "██",
                };
                print!("{}", output);
            }
            println!();
        }
    }

    fn count_alive_neighbors(&self, row_idx: usize, col_idx: usize) -> u8 {
        let mut count: u8 = 0;
        // upX
        if row_idx > 0 {
            // up
            count += u8::from(self.state[row_idx - 1][col_idx]);
            // up left
            if col_idx > 0 {
                count += u8::from(self.state[row_idx - 1][col_idx - 1]);
            }
            // up right
            if col_idx < self.size - 1 {
                count += u8::from(self.state[row_idx - 1][col_idx + 1]);
            }
        }
        // downX
        if row_idx < self.size - 1 {
            // down
            count += u8::from(self.state[row_idx + 1][col_idx]);
            // down left
            if col_idx > 0 {
                count += u8::from(self.state[row_idx + 1][col_idx - 1]);
            }
            // down right
            if col_idx < self.size - 1 {
                count += u8::from(self.state[row_idx + 1][col_idx + 1]);
            }
        }
        // left
        if col_idx > 0 {
            count += u8::from(self.state[row_idx][col_idx - 1]);
        }
        // right
        if col_idx < self.size - 1 {
            count += u8::from(self.state[row_idx][col_idx + 1]);
        }
        count
    }

    fn is_alive(&self, row_idx: usize, col_idx: usize) -> bool {
        let alive = self.state[row_idx][col_idx];
        let alive_neighbors: u8 = self.count_alive_neighbors(row_idx, col_idx);
        alive_neighbors == 3 || (alive && alive_neighbors == 2)
    }

    fn tick(&mut self) {
        let mut new_state: Vec<Vec<bool>> = vec![vec![false; self.size]; self.size];
        (0..self.size).for_each(|row_idx| {
            (0..self.size).for_each(|col_idx| {
                new_state[row_idx][col_idx] = self.is_alive(row_idx, col_idx);
            });
        });
        self.state = new_state;
    }

    fn play(&mut self) {
        let pause = time::Duration::from_millis(1000 / self.tps);
        loop {
            self.draw_board();
            self.tick();
            thread::sleep(pause);
        }
    }
}

fn main() {
    let size = 20;
    let tps = 10;
    let mut game = GameOfLife::new(tps, size);
    game.play();
}
